const readDocument = (app) => async (path) => {
  const doc = await app.firestore().doc(path).get();
  if (!doc.exists) return "Requested Firestore document does not exist";
  return {
    id: doc.id,
    data: doc.data(),
  };
};

module.exports = {
  readDocument,
};
