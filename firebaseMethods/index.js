const firestoreMethods = require("./firestore");

module.exports = {
  ...firestoreMethods,
};
