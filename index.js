const repl = require("repl");

const config = require("./config.json");
const { initializeApps } = require("./loaders");
const { extendWith } = require("./utils");
const firebaseMethods = require("./firebaseMethods");

const apps = initializeApps(config);

const replServer = repl.start(config.inputPrompt);
const context = replServer.context;

const initializeContext = extendWith({ ...apps, ...firebaseMethods });

initializeContext(context);
