const firebase = require("firebase-admin");

const initializeApps = (config) => {
  let initializedApps = {};

  for (let app of config.firebaseApps) {
    const appKey = require(app.pathToKey);

    const appConfig = {
      credential: firebase.credential.cert(appKey),
      databaseURL: app.databaseURL,
    };

    const appSDK = firebase.initializeApp(appConfig, app.name);

    initializedApps = { ...initializedApps, [app.alias]: appSDK };
  }

  console.log(
    `
    The following Firebase apps have been initialized and are available in the REPL: ${Object.keys(initializedApps)}
    `
  );

  return initializedApps;
};

module.exports = { initializeApps };
